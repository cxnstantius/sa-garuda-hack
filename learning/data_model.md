```plantuml

Title User Data Model

class "User.java" as user {
  - long serialVersionUID
  ---
  - Long id
  - String userId
  - String phoneNumber
  - String nickname
  - StatusEnum status
  - Optional<String> address
  - Optional<Date> dob
  - Date registerDate
  ---
  +getter
  +setter
}


```
