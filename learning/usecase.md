```plantuml

title On Boarding Use Cases

actor user

left to right direction

rectangle "WEB" as web {
  user --> (Open On Boarding Page) : Open

  user --> (Registration Page) : Open

  user --> (Login Page) : Open
}

rectangle "Backend" as be{
  (Registration Page) --> (Registration API) : uses
  (Registration Page) --> (Send OTP API) : uses
  (Registration Page) --> (Verify OTP API) : uses

  (Login Page) --> (Login API) : uses

}

```
